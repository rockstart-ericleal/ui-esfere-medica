 <!DOCTYPE html>
<html lang="es">
<head>
	
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Esfera Medica</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="stylesheet" href="./css/style.css">
		
	<!-- <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script> -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="./js/px.js" type="text/javascript"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	
</head>
<body >

		

		<div class="content content-navbar fixed-top mt-lg-2">
			<nav class="navbar navbar-expand-lg navbar-light">
				<a class="navbar-brand color-white" href="#about"><span class="d-block d-lg-none" id="title-menu">NOSOTROS</span></a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarNavDropdown">
					<ul class="navbar-nav ml-auto">
						
						<li class="nav-item dropdown">
							<a class="active nav-link dropdown-toggle  text-right text-lg-left text-uppercase color-white" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Nosotros
							</a>
							<div class="dropdown-menu" aria-labelledby="navbarDropdown">
								<a class="dropdown-item  text-right text-lg-left p-sm-2 py-lg-0 px-lg-2 active" href="#about-us">Quiénes somos</a>
								<a class="dropdown-item  text-right text-lg-left p-sm-2 py-lg-0 px-lg-2" href="#pilares">Pilares</a>									
							</div>
						</li>

						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle  text-right text-lg-left text-uppercase color-white" href="#" id="navbarDropdown-2" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Tecnologías
							</a>
							<div class="dropdown-menu" aria-labelledby="navbarDropdown-2">
								<a class="dropdown-item  text-right text-lg-left p-sm-2 py-lg-0 px-lg-2" href="#metraton-introducer">Metatrón - Intruder</a>
								<a class="dropdown-item  text-right text-lg-left p-sm-2 py-lg-0 px-lg-2" href="#biomecanica-atlaxprofilax">Corrección Biomecánica AtlaxProfilax</a>
								<a class="dropdown-item  text-right text-lg-left p-sm-2 py-lg-0 px-lg-2" href="#sistema-neuroadaptivo">Sistema Neuroadaptivo</a>
								<a class="dropdown-item  text-right text-lg-left p-sm-2 py-lg-0 px-lg-2" href="#monalisa-touch">Monalisa Touch</a>
								<a class="dropdown-item  text-right text-lg-left p-sm-2 py-lg-0 px-lg-2" href="#actividades-plurifuncionales">Actividades Plurifuncionales</a>								
							</div>
						</li>

						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle py-2 text-right text-lg-left text-uppercase color-white" href="#" id="navbarDropdown-3" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Especialistas
							</a>
							<div class="dropdown-menu" aria-labelledby="navbarDropdown-3">
								<a class="dropdown-item  text-right text-lg-left p-sm-2 py-lg-0 px-lg-2" href="#dr-Agustin-gomez">Dr. Agustín Gómez</a>
								<a class="dropdown-item  text-right text-lg-left p-sm-2 py-lg-0 px-lg-2" href="#dr-ignacio-martinez">Dr. Ignacio Martínez</a>						
							</div>
						</li>


						<li class="nav-item">
							<a class="menu-alone nav-link  text-right text-lg-left text-uppercase color-white" href="#blog">Blog</a>
						</li>

						<li class="nav-item">
							<a class="menu-alone nav-link  text-right text-lg-left text-uppercase color-white" href="#video-blog">Videoblog</a>
						</li>

						<li class="nav-item">
							<a class="menu-alone nav-link  text-right text-lg-left text-uppercase color-white" href="#faqs">FAQs</a>
						</li>

						<li class="nav-item">
							<a class="menu-alone nav-link  text-right text-lg-left text-uppercase color-white" href="#contact">Contacto</a>
						</li>

					</ul>
				</div>
			</nav>
		</div>
		

		<!-- Content Blog -->
		<section id="about-us">
			<div class="container-fluid no-gutters" >
				<div class="row h-full">
					
					<div class="col-6 d-none d-lg-block">
						<div class="row h-100">
							<div class="col-12 align-self-center text-right">
								<img class="img-cover-post " src="https://via.placeholder.com/780x512">
							</div>
						</div>
					</div>

					<div class="col-12 col-lg-6 mt-5mt-5 overflow-y-scroll">
						<div class="container">

							<div class="space-white"></div>

							<div class="col-12 d-sm-block d-lg-none text-center">
								<img class="img-cover-post p-4" src="https://via.placeholder.com/780x512">
							</div>

							<h1 class="text-center mb-4 rem-2 fra mark-menu-about"  title-mb="NOSOTROS" >Quiénes Somos</h1>
							<div class="container">
								<div class="row  justify-content-center ">

									<div class="col-12 col-md-10">
										<p class="text-left mpro-rg text-justify">Con el paso de tiempo, la menopausia y los partos, la vagina tiende a perder firmeza y a envejecer1, lo que supone una serie de afectaciones en el estado físico y emocional de las mujeres. Sin embargo, en la actualidad existen innovadoras tecnologías de la medicina no invasiva que ayudan a rejuvenecer la zona íntima femenina y revertir estos problemas.</p>
										
									</div>	
								</div>
							</div>

							<div class="space-white"></div>
							
						</div>
					</div>
				</div>
			</div>
		</section>



		
	    
		<script type="text/javascript">
			  
		    $(document).ready(function(){
				// Add smooth scrolling to all links
				$("a").on('click', function(event) {

				// Make sure this.hash has a value before overriding default behavior
				if (this.hash !== "") {

				  console.log(this.hash)

				  var itemsMenu = $('.dropdown-item');
				  itemsMenu.each(function(index, el) {
					$(el).removeClass('active')
				  });

				  var itemsMenuAlone = $('.menu-alone');
				  itemsMenuAlone.each(function(index, el) {
					$(el).removeClass('active')
				  });

				  $('[href="'+this.hash+'"]').addClass('active');

				  // Prevent default anchor click behavior
				  event.preventDefault();

				  // Store hash
				  var hash = this.hash;


				  // Using jQuery's animate() method to add smooth page scroll
				  // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
				  $('html, body').animate({
				    scrollTop: $(hash).offset().top
				  }, 600, function(){

				    // Add hash (#) to URL when done scrolling (default click behavior)
				    window.location.hash = hash;
				  });
				} // End if
				});

				(function($) {
				    $.fn.hasScrollBar = function() {
				        return this.get(0).scrollHeight > this.height();
				    }
				})(jQuery);

				function showSecction(elem){
					var element = document.querySelector(elem);
					var position = element.getBoundingClientRect();					
					if(position.top >= 0 && position.bottom <= window.innerHeight) {						
						$('#title-menu').text($(elem).attr('title-mb'));
					}
					if(position.top < window.innerHeight && position.bottom >= 0) {						
						$('#title-menu').text($(elem).attr('title-mb'));
					}
				}

				window.addEventListener('scroll', function() {
					
					showSecction('.mark-menu');
					showSecction('.mark-menu-about');
					showSecction('.mark-menu-about-1');
					showSecction('.mark-menu-tecno-1');
					showSecction('.mark-menu-tecno-2');
					showSecction('.mark-menu-tecno-3');
					showSecction('.mark-menu-tecno-4');
					showSecction('.mark-menu-tecno-5');
					showSecction('.mark-menu-especial-1');
					showSecction('.mark-menu-especial-2');
					showSecction('.mark-menu-prensa-1');
					showSecction('.mark-menu-blog-1');
					showSecction('.mark-menu-faqs-1');
					showSecction('.mark-menu-contact-1');
					showSecction('.mark-menu-videoblog-1');	
					

				});


				function checkScreenSize(size, minBG, maxBG) {
				  var x = window.matchMedia(size)
				  if (x.matches) { // If media query matches		
				    minBG();
				  } else {				
				  	maxBG();
				  }
				  x.addListener(checkScreenSize)
				}
				

				checkScreenSize('(max-width: 700px)',function(){
						
					$('#dr-Agustin-gomez').parallax({
						position:'650px 0px',
						imageSrc:'./img/9mobile.jpg'
					});
					$('#dr-ignacio-martinez').parallax({
						position:'650px 0px',
						imageSrc:'./img/10mobile.jpg'
					});
					$('#monalisa-touch').parallax({
						position:'660px 0px',
						imageSrc:'./img/7mobile.jpg'
					});

				},function(){
					
					$('#dr-Agustin-gomez').parallax({
						imageSrc:'./img/9.png'
					});
					$('#dr-ignacio-martinez').parallax({
						imageSrc:'./img/10.png'
					});
					$('#monalisa-touch').parallax({
						imageSrc:'./img/7.png'
					});

				}); 

				
				$('.dropdown-item').on('click', function(){
				    $('.navbar-collapse').collapse('hide');
				});

				$('.menu-alone').on('click', function(){
				    $('.navbar-collapse').collapse('hide');
				});


				var itemsVideoBlog = $('.item-video-blog-em');
				var itemsVideoBlogFirst = $(itemsVideoBlog[0]);
				$('#box-video').attr('src',itemsVideoBlogFirst.attr('youtube-url') );

				itemsVideoBlog.click( function(event) {					
					
					var currentElement = $(this);

					itemsVideoBlog.each(function(index, el) {
						$(el).removeClass('video-active')
					});

					currentElement.addClass('video-active');
					var url = currentElement.attr('youtube-url');

					$('#box-video').attr('src',url);
				});


			});

		</script>


</body>
</html>