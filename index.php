 <!DOCTYPE html>
<html lang="es">
<head>
	
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Esfera Medica</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="stylesheet" href="./css/style.css">
		
	<!-- <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script> -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="./js/px.js" type="text/javascript"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	
</head>
<body onload="loadScreen()">

	<!-- <div class="init-load">
		<div class="logo-splash-screen"></div>
		<div class="progress-horizontal">
		  <div class="bar-horizontal"></div>
		</div>
	</div> -->

		<!-- Menu -->
		<div class="content content-navbar fixed-top mt-lg-2">
			<nav class="navbar navbar-expand-lg navbar-light">
				<a class="navbar-brand color-white" href="#about"><span class="d-block d-lg-none" id="title-menu">NOSOTROS</span></a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarNavDropdown">
					<ul class="navbar-nav ml-auto">
						
						<li class="nav-item dropdown">
							<a class="active nav-link dropdown-toggle  text-right text-lg-left text-uppercase color-white" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Nosotros
							</a>
							<div class="dropdown-menu" aria-labelledby="navbarDropdown">
								<a class="dropdown-item  text-right text-lg-left p-sm-2 py-lg-0 px-lg-2 active" href="#about-us">Quiénes somos</a>
								<a class="dropdown-item  text-right text-lg-left p-sm-2 py-lg-0 px-lg-2" href="#pilares">Pilares</a>									
							</div>
						</li>

						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle  text-right text-lg-left text-uppercase color-white" href="#" id="navbarDropdown-2" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Tecnologías
							</a>
							<div class="dropdown-menu" aria-labelledby="navbarDropdown-2">
								<a class="dropdown-item  text-right text-lg-left p-sm-2 py-lg-0 px-lg-2" href="#metraton-introducer">Metatrón - Intruder</a>
								<a class="dropdown-item  text-right text-lg-left p-sm-2 py-lg-0 px-lg-2" href="#biomecanica-atlaxprofilax">Corrección Biomecánica AtlaxProfilax</a>
								<a class="dropdown-item  text-right text-lg-left p-sm-2 py-lg-0 px-lg-2" href="#sistema-neuroadaptivo">Sistema Neuroadaptivo</a>
								<a class="dropdown-item  text-right text-lg-left p-sm-2 py-lg-0 px-lg-2" href="#monalisa-touch">Monalisa Touch</a>
								<a class="dropdown-item  text-right text-lg-left p-sm-2 py-lg-0 px-lg-2" href="#actividades-plurifuncionales">Actividades Plurifuncionales</a>								
							</div>
						</li>

						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle py-2 text-right text-lg-left text-uppercase color-white" href="#" id="navbarDropdown-3" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Especialistas
							</a>
							<div class="dropdown-menu" aria-labelledby="navbarDropdown-3">
								<a class="dropdown-item  text-right text-lg-left p-sm-2 py-lg-0 px-lg-2" href="#dr-Agustin-gomez">Dr. Agustín Gómez</a>
								<a class="dropdown-item  text-right text-lg-left p-sm-2 py-lg-0 px-lg-2" href="#dr-ignacio-martinez">Dr. Ignacio Martínez</a>						
							</div>
						</li>


						<li class="nav-item">
							<a class="menu-alone nav-link  text-right text-lg-left text-uppercase color-white" href="#blog">Blog</a>
						</li>

						<li class="nav-item">
							<a class="menu-alone nav-link  text-right text-lg-left text-uppercase color-white" href="#video-blog">Videoblog</a>
						</li>

						<li class="nav-item">
							<a class="menu-alone nav-link  text-right text-lg-left text-uppercase color-white" href="#faqs">FAQs</a>
						</li>

						<li class="nav-item">
							<a class="menu-alone nav-link  text-right text-lg-left text-uppercase color-white" href="#contact">Contacto</a>
						</li>

					</ul>
				</div>
			</nav>
		</div>

		<!-- <div class="content content-navbar fixed-top mt-lg-2">
		  <nav class="navbar navbar-expand-lg navbar-light">
		  	
		    <a class="navbar-brand color-white" href="#about"><span class="d-block d-lg-none" id="title-menu">NOSOTROS</span></a>
		    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		    </button>
		    <div id="navbarNavDropdown" class="collapse navbar-collapse">
		      <ul id="menu-menu-principal" class="navbar-nav ml-auto">
		        <li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-8" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children dropdown menu-item-8 nav-item">
		          <a title="Nosotros" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle nav-link text-uppercase" id="menu-item-dropdown-8">Nosotros</a>
		          <ul class="dropdown-menu" aria-labelledby="menu-item-dropdown-8" role="menu">
		            <li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-9" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-9 nav-item"><a title="Quienes Somos" href="#quines" class="dropdown-item">Quiénes Somos</a></li>
		            <li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-10" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-10 nav-item"><a title="Pilares" href="#pilares" class="dropdown-item">Pilares</a></li>
		          </ul>
		        </li>
		        <li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-11" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children dropdown menu-item-11 nav-item">
		          <a title="Tecnologías" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle nav-link text-uppercase" id="menu-item-dropdown-11">Tecnologías</a>
		          <ul class="dropdown-menu" aria-labelledby="menu-item-dropdown-11" role="menu">
		            <li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-12" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-12 nav-item"><a title="Tecnologia 1" href="#T1" class="dropdown-item">Metatrón - Intruder</a></li>
		            <li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-13" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13 nav-item"><a title="Tesnologias 2" href="#T2" class="dropdown-item">Correción Biomécanica AtlaxProfilax</a></li>
		            <li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-13" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13 nav-item"><a title="Tesnologias 2" href="#T2" class="dropdown-item">Sistema Electroestimulador</a></li>
		            <li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-13" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13 nav-item"><a title="Tesnologias 2" href="#T2" class="dropdown-item">Monalisa Touch</a></li>
		            <li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-13" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13 nav-item"><a title="Tesnologias 2" href="#T2" class="dropdown-item">Actividades Plurifuncionales</a></li>
		          </ul>
		        </li>
		        <li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-14" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-14 nav-item"><a title="BLOG" href="#BLOG" class="nav-link">BLOG</a></li>
		        <li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-15" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-15 nav-item text-uppercase"><a title="video blog" href="#video" class="nav-link">video blog</a></li>
		        <li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-15" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-15 nav-item text-uppercase"><a title="video blog" href="#video" class="nav-link">FAQs</a></li>
		        <li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-16" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-16 nav-item"><a title="Contacto" href="#contacto" class="nav-link text-uppercase">Contacto</a></li>
		      </ul>
		    </div>
		  </nav>
		</div> -->
		
		<!-- Cover -->
		<section id="about" data-parallax="scroll" data-image-src="./img/bg-1.jpg">
			<div class="container-fluid no-gutters position-relative " >
				<div class="position-absolute box-icon-container">
					<figure class="mark-menu" title-mb="NOSOTROS">
						<img class="log-icon-em" src="./img/logo-em.png">
					</figure>
				</div>
				<div class="box-menu-container position-absolute d-none d-lg-block ">
					<ul>
						<li>
							<a href="">
								<img src="./img/icons/icon-fb.svg">
							</a>
						</li>
						<li>
							<a href="">
								<img src="./img/icons/icon-tw.svg">
							</a>
						</li>
						<li>
							<a href="">
								<img src="./img/icons/icon-ig.svg">
							</a>
						</li>
						<li>
							<a href="">
								<img src="./img/icons/icon-in.svg">
							</a>
						</li>
						<li>
							<a href="">
								<img src="./img/icons/icon-spfy.svg">
							</a>
						</li>
						<li>
							<a href="">
								<img src="./img/icons/icon-ytb.svg">
							</a>
						</li>
						<!-- <li>
							<a href="">
								<img src="./img/icons/icon-ws.svg">
							</a>
						</li> -->
					</ul>
				</div>
				<div class="row h-full"></div>
			</div>
		</section>

		<!-- About -->
		<section class="position-relative" id="about-us" data-parallax="scroll" data-image-src="./img/bg-2.jpg">
			

			<figure class="position-absolute icon-logo-em-right ">
				<img class="icon-logo-about" src="./img/logo-em-2.png">
			</figure>

			<div class="container-fluid no-gutters" >
				<div class="row h-full pt-5">
					<div class="col-6 d-none d-lg-block"></div>
					<div class="col-12 col-lg-6 my-auto mt-5">
						<div class="container">
							<h1 class="text-center mb-4 rem-2-8 mark-menu-about mpro-bold"  title-mb="NOSOTROS" >Quiénes Somos</h1>
							<div class="container">
								<div class="row  justify-content-center">
									<div class="col-12 col-md-8 mpro-rg">
										<p class="text-justify"><b class="mpro-bold">ESFERA MÉDICA</b> es un concepto que integra tecnologías innovadoras de la medicina no invasiva probadas a nivel mundial que ayudan a hacer un diagnóstico.</p>
										<p class="text-justify">En <b class="mpro-bold">ESFERA MÉDICA</b> contamos con profesionales de la salud que han sido capacitados en el extranjero para brindar atención personalizada y de calidad a nuestros pacientes bajo el precepto de <i>“la combinación del uso de tecnología y calidez humana cerca de ti”</i>.</p>
									</div>	
								</div>
							</div>
							
						</div>
					</div>
				</div>
			</div>
		</section>

		<!-- Pilares -->
		<section class="position-relative" id="pilares" data-parallax="scroll" data-image-src="./img/bg-3.jpg">
			
			<figure class="position-absolute icon-logo-em-left">
				<img class="icon-logo-about" src="./img/logo-em-2.png">
			</figure>

			<div class="container-fluid no-gutters" >
				<div class="row h-full">
					<div class="col-12 col-lg-6 my-auto">
						<div class="container">
							
							
							<div class="row justify-content-center">
								<div class="col-12 col-md-10 px-0 px-md-5">
									<h1 class="text-left rem-2 mpro-bold pl-4 mark-menu-about-1"  title-mb="NOSOTROS" >Nuestra Misión</h1>
									<p class="mpro-rg text-justify">Integrar innovadoras tecnologías de diferentes partes del mundo para brindar ayuda en diagnósticos y tratamientos efectivos y personalizados para mejorar la salud de cada paciente.</p>
									
									<h1 class="text-left rem-2 mpro-bold pl-4">Nuestra Visión</h1>
									<p class="text-left mpro-rg">Ser agentes de cambio hacia el nuevo paradigma de salud (no invasiva) centrada en el paciente.</p>
									
									<h1 class="text-left rem-2 mpro-bold pl-4">Nuestros Valores</h1>
									<ul class="mpro-rg pres-list">
										<li>Integridad</li>
										<li>Excelencia</li>
										<li>Innovación</li>
									</ul>
								</div>	
							</div>

						</div>
					</div>
					<div class="col-6 d-none d-lg-block"></div>
				</div>
			</div>
		</section>

		<!-- Metraton - Introducer -->
		<section id="metraton-introducer" data-parallax="scroll" data-image-src="./img/bg-4.png">
			<div class="container-fluid no-gutters position-relative" >

			<figure class="position-absolute icon-logo-em-right">
				<img class="icon-logo-about" src="./img/logo-em-2.png">
			</figure>

				<div class="row h-full">
					<div class="col-12 col-lg-6 my-auto">
						<div class="container">
							
							<div class="row justify-content-center">
								<div class="col-12 col-md-10 px-0 px-md-5">
									<h1 class="text-center rem-2-3  mpro-bold mark-menu-tecno-1"  title-mb="TECNOLOGÍAS">Metatrón - Intruder</h1>
									<p class="text-justify mpro-rg">La tecnología rusa Intruder (evolución de Metatrón), es un sistema tenológico de apoyo no invasivo basado en la Resonancia Magnética No Lineal. Se trata de un método con capacidad para procesar datos de análisis no lineal, que permite obtener en una evaluación funcional del organismo de manera general con resultados útiles para los médicos de manera casi inmediata.</p>

									
									<div class="icon-add-circle button-show-list"></div>

									<div class="container-info">
										<h1 class="text-left rem-1 mpro-bold">Ventajas:</h1>
										<ul class="mpro-rg">
											<li>Diagnósticos temprano de patologías.</li>
											<li>Ayuda a definir un diagnóstico del malestar.</li>
											<li>Predice tendencias a disfunciones y predisposiciones a padecimientos.</li>
											<li>Evalúa parámetros básicos de indicadores bioquímicos como plasma y orina.</li>
											<li>Valora previamente efectividad de diferentes terapias.</li>
											<li>Prueba la eficacia de medicamentos, complementos, alérgenos y alimentos.</li>
											<li>Detecta y tipifica de microorganismos asociados y no asociados al desarrollo del proceso patológico.</li>
											<li>Analiza la dinámica de cambio del estado funcional del organismo durante un tratamiento. </li>
											<li>Evaluación de órganos.</li>
											<li>Rápido y cómodo.</li>
											<li>Mayor capacidad para recabar información.</li>
											<li>Patologías indistintas.</li>
											<li>Mayor rango de edad para el análisis.</li>
										</ul>
									</div>
										</div>
							</div>

						</div>
					</div>
					<div class="col-6 d-none d-lg-block"></div>
				</div>
			</div>
		</section>


		<!-- Correccion Biomecanica AtlaxProfilax -->
		<section class="position-relative" id="biomecanica-atlaxprofilax" data-parallax="scroll" data-image-src="./img/bg-5.jpg">
			
			<figure class="position-absolute icon-logo-em-left">
				<img class="icon-logo-about" src="./img/logo-em-2.png">
			</figure>

			<div class="container-fluid no-gutters " >
				<div class="row h-full">
					<div class="col-12 col-lg-6 my-auto">
						<div class="container fra mt-5">
							
							<div class="row justify-content-center">
								<div class="col-12 col-md-10 mpro-rg px-0 px-md-5">
									<h1 class="text-center rem-2-3 mark-menu-tecno-2 mpro-bold"  title-mb="TECNOLOGÍAS">Corrección Biomecánica AtlasProfilax</h1>
									<p class="text-justify">Es una técnica que a través de los músculos y fascias musculares de cuello ayuda a identificar las diferentes cargas o los diferentes ejes postulares incorrectos.</p>
									<p class="text-justify">Esta técnica tiene como objetivo el de ayudar a corregir y alinear la postura a través de las fascias musculares que nacen en el cuello y es suficiente realizarlo sólo una vez (en la mayoría de los casos).</p>
									<p class="text-justify">Una vez que cuerpo desde las fascias del cuello y sus ejes como mandíbula, cuello, hombros, cadera, rodillas y tobillos se alinean ayudamos a eliminar un alto porcentaje de los dolores crónicos osteomusculares.</p>

								</div>
							</div>

						</div>
					</div>
					<div class="col-6 d-none d-lg-block"></div>
				</div>
			</div>
		</section>


		<!-- Sistema Neuroadaptivo -->
		<section class="position-relative" id="sistema-neuroadaptivo" data-parallax="scroll" data-image-src="./img/bg-6.png">
			
			<figure class="position-absolute icon-logo-em-left">
				<img class="icon-logo-about" src="./img/logo-em-2.png">
			</figure>

			<div class="container-fluid no-gutters" >
				<div class="row h-full">
					<div class="col-12 col-lg-6 my-auto">
						<div class="container fra mark-menu-tecno-5"  title-mb="TECNOLOGÍAS">
							
							<div class="row justify-content-center">
								<div class="col-12 col-md-12 px-0 px-md-5 mpro-rg">
									<h1 class="text-center mpro-bold rem-2-3">Sistema Electroestimulador</h1>
									<p class="text-justify px-lg-5">Método de apoyo de electroestimulación para proporcionar tratamiento no invasivo terapéutico general a los sistemas fisiológicos del cuerpo por medio de las áreas de la piel humana para poder tratar diferentes patologías.</p>
									<!-- <p class="text-justify">Su uso es recomendado en neurología.</p> -->
									
								</div>	
							</div>


						</div>
					</div>
					<div class="col-6 d-none d-lg-block"></div>
				</div>
			</div>
		</section>

		<!-- Monalisa Touch -->
		<section id="monalisa-touch" class="position-relative">

			<figure class="position-absolute icon-logo-em-left">
				<img class="icon-logo-about" src="./img/logo-em-2.png">
			</figure>

			<div class="container-fluid no-gutters " >
				<div class="row h-full">
					<div class="col-12 col-lg-6 my-auto">
						<div class="container fra mark-menu-tecno-4"  title-mb="TECNOLOGÍAS">
							
							<div class="row justify-content-center">
								<div class="col-12 col-md-8 mpro-rg">
									<h1 class="text-center rem-2-8 mpro-bold">Monalisa Touch</h1>
									<p class="text-justify">Es el primer láser de apoyo ginecológico específicamente diseñado para tratar algunas de las disfunciones vulvovaginales. Este sistema emplea un innovador láser de CO2 fraccional que provoca una contracción inmediata de las paredes vaginales, favoreciendo la producción de colágeno, incrementa vascularización sanguínea y permeabilidad de las mucosas, creando un verdadero rejuvenecimiento vulvovaginal, que permite la recuperación de la funcionalidad, elasticidad y lubricación de la vagina.</p>
									<p class="text-justify">Es un tratamiento mínimamente invasivo, en ocasiones indoloro o de molestias ligeras (puede variar de persona en persona).</p>									
								</div>	
							</div>

							
						</div>
					</div>
					<div class="col-6 d-none d-lg-block"></div>
				</div>
			</div>
		</section>

		<!-- Actividades Plurifuncionales -->
		<section class="position-relative" id="actividades-plurifuncionales" data-parallax="scroll" data-image-src="./img/8.png">
			
			<figure class="position-absolute icon-logo-em-left">
				<img class="icon-logo-about" src="./img/logo-em-2.png">
			</figure>

			<div class="container-fluid no-gutters" >
				<div class="row h-full pt-5">
					<div class="col-12 h-100 col-lg-6 my-auto">
						<div class="container fra">
							
							<div class="row justify-content-center">
								<div class="col-12 col-md-10 mpro-rg">
									<h1 class="text-center rem-2 mark-menu-tecno-3 mpro-bold"  title-mb="TECNOLOGÍAS">Actividades Plurifuncionales</h1>
									<div class="px-0 px-md-5">
										<p class="text-justify">El equilibrio de la boca es más que dientes alineados, es función y forma de todo el cuerpo. La mala alineación bucal puede provocar tensión y dolor muscular, falta de concentración, apariencia desarmonizada o envejecida, mala postura y pisada, problemas digestivos, bajo rendimiento y migraña.</p>
										<p class="text-justify">Somos los únicos representantes certificados en México y Latinoamérica del método de apoyo francés de auto-activación con Activadores Plurifuncionales, los cuales ayudan a mantener la lengua en una posición correcta, restablecer la respiración nasal, abre las vías respiratorias, aumenta la dimensión cervical favoreciendo la función y apariencia, funciona como guía a los dientes que están por erupcionar, impulsa la mandíbula hacia delante liberando tensión, y mejora la oxigenación y circulación.</p>
									
									</div>
								</div>
							</div>
							
						</div>
					</div>
					<div class="col-6 d-none d-lg-block"></div>
				</div>
			</div>
		</section>

		<!-- Dr. Agustín Gómez  data-parallax="scroll" data-image-src="./img/9.png"-->
		<section class="position-relative" id="dr-Agustin-gomez" >

			<figure class="position-absolute icon-logo-em-left">
				<img class="icon-logo-about" src="./img/logo-em-2.png">
			</figure>

			<div class="container" >			

				<div class="row h-full">
					<div class="col-12 col-lg-6 my-auto">
						<div class="container fra mark-menu-especial-1"  title-mb="ESPECIALISTAS">
							
							<div class="row justify-content-center">
								<div class="col-12 col-md-10 mpro-rg">
									<h1 class="text-center rem-2-8 mpro-bold">Dr. Agustín Gómez</h1>
									<h3 class="text-center rem-1-2 mb-4">Cédula profesional <b class="mpro-bold">IPN 3973092</b></h3>
									<p class="text-justify">Es egresado del Instituto Politécnico Nacional (IPN). Su interés como médico por encontrar las causas de las enfermedades lo ha llevado a explorar diferentes métodos y tratamientos.</p>
									<p class="text-justify">En el año 2000 inició uno de sus proyectos más importantes que ha marcado su vida profesional: Esfera Médica, una clínica especializada de atención médica y enfocada en concentrar nuevas tecnologías médicas a nivel mundial que apoyan a encontrar las causas de las enfermedades. Tecnologías innovadoras que hoy en día, a su consideración, han empezado a marcar la pauta de la medicina del futuro.</p>
									<p class="text-justify">Esfera Médica trabaja bajo el precepto de la innovación, ofreciendo a sus pacientes nuevas tecnologías complementarias a sus tratamientos médicos, que buscan mejorar su salud y calidad de vida.</p>
										
								</div>	
							</div>

						</div>
					</div>
					<div class="col-6 d-none d-lg-block"></div>
				</div>
			</div>
		</section>


		<!-- Dr. Ignacio Martínez -->
		<section class=" position-relative" id="dr-ignacio-martinez">

			<figure class="position-absolute icon-logo-em-left">
				<img class="icon-logo-about" src="./img/logo-em-2.png">
			</figure>

			<div class="container" >

				

				<div class="row h-full">
					<div class="col-12 col-lg-6 my-auto">
						<div class="container fra mark-menu-especial-2"  title-mb="ESPECIALISTAS">
							
							<div class="row justify-content-center">
								<div class="col-12 col-md-10 mpro-rg">
									<h1 class="text-center rem-2-5 mpro-bold">Dr. Ignacio Martínez</h1>
									<h3 class="text-center rem-1-2 mb-4">Cédula profesional <b class="mpro-bold">UAM 2282844</b></h3>
									<p class="text-justify">El Dr. Ignacio Martínez es un médico cirujano por la Universidad Autónoma Metropolitana (UAM). Apasionado de su profesión y líder en innovación al combinar diversos métodos de medicina alternativa con nuevas tecnologías científicas.</p>
									<p class="text-justify">Sus logros profesionales están ligados a su visión de la vida: plantear y cumplir objetivos. Gracias a su intensiva preparación, carisma y talento, es un exitoso médico que ha logrado cumplir todos sus sueños, ayudando a sus pacientes a encontrar la salud, compartiendo su conocimiento e investigación, y capacitándose todos los días para ofrecer lo mejor del mundo a las personas que están cerca de él.</p>
									<p class="text-justify">En busca de la innovación, el Dr. Ignacio Martínez ha explorado incansablemente diversos caminos de las ciencia y la tecnología médica; esto lo ha llevado a traer a México nuevas tecnologías a nivel mundial que ha puesto al alcance de todo el público de nuestro país a través de Esfera Médica, su proyecto de vida personal y profesional.</p>
								
								</div>
							</div>
							
						</div>
					</div>
					<div class="col-6 d-none d-lg-block"></div>
				</div>
			</div>
		</section>


		<!-- Prensa! -->
		<!-- <section id="prensa" class="mark-menu-prensa-1"  title-mb="PRENSA">
			<div class="container-fluid no-gutters prensa fra" > -->

				<!-- <figure class="position-absolute icon-logo-em-right d-none d-lg-block">
					<img class="icon-logo-about mt-2 mb-2 min-icon-logo" src="https://via.placeholder.com/700x480">
				</figure> -->

<!-- 				<div class="row h-full" >
					<div class="col-12 align-self-center  mt-5 pt-5">
						
						<div class="row justify-content-center " >
							
							<div class="col-sm-12 col-md-5 col-lg-5 mt-4 mt-lg-5">														
								<div class="row justify-content-center">
									<div class="col-6 col-lg-4  position-relative align-self-end">
										<a href="#">
											<div class="position-absolute rounded-circle bg-item-link"></div>
										</a>
										<img class="icon-item-media rounded-circle" src="https://via.placeholder.com/512">
									</div>
									<div class="col-6 col-lg-4  position-relative">
										<div class="h-100">
											<div class="row h-100">
												<div class="col-12 align-self-center">
													<h3 class="rem-2 ml-2">Prensa</h3>
												</div>
											</div>
										</div>
									</div>
								</div>							
							</div>

							<div class="col-sm-12 col-md-5 col-lg-5 mt-4 mt-lg-5">														
								<div class="row justify-content-center">
									<div class="col-6 col-lg-4  position-relative align-self-end">
										<a href="#">
											<div class="position-absolute rounded-circle bg-item-link"></div>
										</a>
										<img class="icon-item-media rounded-circle" src="https://via.placeholder.com/512">
									</div>
									<div class="col-6 col-lg-4  position-relative">
										<div class="h-100">
											<div class="row h-100">
												<div class="col-12 align-self-center">
													<h3 class="rem-2 ml-2">T.V.</h3>
												</div>
											</div>
										</div>
									</div>
								</div>							
							</div>

						</div>

						<div class="row justify-content-center">
							
							<div class="col-sm-12 col-md-5 col-lg-5 mt-4 mt-lg-5">														
								<div class="row justify-content-center">
									<div class="col-6 col-lg-4  position-relative align-self-end">
										<a href="#">
											<div class="position-absolute rounded-circle bg-item-link"></div>
										</a>
										<img class="icon-item-media rounded-circle" src="https://via.placeholder.com/512">
									</div>
									<div class="col-6 col-lg-4  position-relative">
										<div class="h-100">
											<div class="row h-100">
												<div class="col-12 align-self-center">
													<h3 class="rem-2 ml-2">Radio</h3>
												</div>
											</div>
										</div>
									</div>
								</div>							
							</div>

							<div class="col-sm-12 col-md-5 col-lg-5 mt-4 mt-lg-5">														
								<div class="row justify-content-center">
									<div class="col-6 col-lg-4  position-relative align-self-end">
										<a href="#">
											<div class="position-absolute rounded-circle bg-item-link"></div>
										</a>
										<img class="icon-item-media rounded-circle" src="https://via.placeholder.com/512">
									</div>
									<div class="col-6 col-lg-4  position-relative">
										<div class="h-100">
											<div class="row h-100">
												<div class="col-12 align-self-center">
													<h3 class="rem-2 ml-2">Digital</h3>
												</div>
											</div>
										</div>
									</div>
								</div>							
							</div>


						</div>

						<div class="row">
							<div class="col-12">
								<figure class="text-center">
									<img class="icon-logo-footer-section mt-4 mb-4" src="./img/logo-em.png">
								</figure>
							</div>
						</div>



					</div>
				</div>
			</div>
		</section> -->

		<!-- BLOG! -->
		<section id="blog">
			<div class="container-fluid no-gutters  mpro-rg position-relative" >
			
				<figure class="position-absolute icon-logo-em-left">
					<img class="icon-logo-about" src="./img/logo-em-2.png">
				</figure>
				
				<div class="container">
					<div class="row h-full pb-5 mark-menu-blog-1"  title-mb="BLOG">
						<div class="col-12 align-self-center my-5">
							<div class="row box-post-container justify-content-center mb-5">


								<? for($i = 0; $i < 3; $i++): ?>
								<!-- START ITEM POST LINK --> 
								<div class="col-12 col-md-6 col-lg-4 ">
									<div class="row m-2">
										<div class="col-12 box-container-img-post-em position-relative">
											<a target="_blank" href="/blog.php">
												<div class="layer-hover-post position-absolute"></div>
												<img class="img-post-em" src="https://via.placeholder.com/1200x675">
											</a>
										</div>
										<div class="col-12">
											<h1 class="rem-1-2 text-center mt-2 mpro-bold">¿Por qué es importante corregir la postura del cuerpo?</h1>
										</div>
										<div class="col-12 text-right pr-0 rem-1-2">
											<a href="#">Leer más</a>
										</div>
									</div>
								</div>
								<!-- END ITEM POST LINK --> 
								<? endfor ?>


							</div>
						</div>
					</div>
					</div>

			</div>
		</section>

		<!-- Video Blog! -->
		<section id="video-blog">
			<div class="container  h-full video-blog-em position-relative" >

				<!-- <figure class="position-absolute icon-logo-em-right d-none d-md-block">
					<img class="icon-logo-about mt-2 mb-2 min-icon-logo" src="./img/logo-em.png">
				</figure> -->

				<div class="row h-full mark-menu-videoblog-1"  title-mb="VIDEOBLOG" >
					
					<!-- video-active  -->
					<div class="pt-5 overflow-y-scroll col-12 col-md-4  order-2 order-md-1 align-self-center h-full bg-grey">						
						
						<ul>
							<li class="m-4"><img class="video-active item-video-blog-em" src="https://via.placeholder.com/560x315" youtube-url="https://www.youtube.com/embed/VE_pN7A1fWY"></li>
							<li class="m-4"><img class="item-video-blog-em" src="https://via.placeholder.com/560x315" youtube-url="https://www.youtube.com/embed/SCztMaWSGoM"></li>
							<li class="m-4"><img class="item-video-blog-em" src="https://via.placeholder.com/560x315" youtube-url="https://www.youtube.com/embed/VE_pN7A1fWY"></li>
							<li class="m-4"><img class="item-video-blog-em" src="https://via.placeholder.com/560x315" youtube-url="https://www.youtube.com/embed/SCztMaWSGoM"></li>
							<li class="m-4"><img class="item-video-blog-em" src="https://via.placeholder.com/560x315" youtube-url="https://www.youtube.com/embed/VE_pN7A1fWY"></li>
							<li class="m-4"><img class="item-video-blog-em" src="https://via.placeholder.com/560x315" youtube-url="https://www.youtube.com/embed/OAEkR4nLq_s"></li>
							<li class="m-4"><img class="item-video-blog-em" src="https://via.placeholder.com/560x315" youtube-url="https://www.youtube.com/embed/VE_pN7A1fWY"></li>
							<li class="m-4"><img class="item-video-blog-em" src="https://via.placeholder.com/560x315" youtube-url="https://www.youtube.com/embed/OAEkR4nLq_s"></li>
							<li class="m-4"><img class="item-video-blog-em" src="https://via.placeholder.com/560x315" youtube-url="https://www.youtube.com/embed/VE_pN7A1fWY"></li>
							<li class="m-4"><img class="item-video-blog-em" src="https://via.placeholder.com/560x315" youtube-url="https://www.youtube.com/embed/OAEkR4nLq_s"></li>
							<li class="m-4"><img class="item-video-blog-em" src="https://via.placeholder.com/560x315" youtube-url="https://www.youtube.com/embed/VE_pN7A1fWY"></li>
						</ul>
					
					</div>
					<!-- video-active  -->

					<div class="box-video-content col-12 col-md-8 order-1 order-md-2 align-self-center text-center">
						
						<div class="">
							<iframe id="box-video" class="video-youtube-em" width="560" height="415" src="https://www.youtube.com/embed/VE_pN7A1fWY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>							
						</div>
						
					</div>
				</div>
			</div>
		</section>

		<!-- FAQs -->
		<section class="position-relative" id="faqs" data-parallax="scroll" data-image-src="./img/bg-11.jpg">
			<div class="container-fluid  no-gutters" >

			<figure class="position-absolute icon-logo-em-right">
				<img class="icon-logo-about" src="./img/logo-em-2.png">
			</figure>

				<div class="row h-full ">
					<div class="col-12 col-lg-8 my-auto mark-menu-faqs-1"  title-mb="FAQs">
						<div class="container mt-5 pt-2 mpro-normal">
							
							<div class="row justify-content-center">
								<div class="col-12 col-md-9">

									<ul class="list-faqs">
										
										<li>¿Atienden a todo tipo de pacientes?</li>
										<p class="text-left rem--7">En Esfera Médica deseamos ayudar a todo tipo de pacientes; sin embargo, es hasta que les brindamos un diagnóstico correcto que podemos determinar si el tratamiento puede ser atendido por nosotros o algún otro especialista.</p>
										
										<li>¿Necesitaré estar internado para recibir el tratamiento?</li>
										<p class="text-left rem--7">No, nuestros tratamientos no requieren que sea internado.</p>

										<li>¿Cómo sé si el tratamiento está funcionando?</li>
										<p class="text-left rem--7">Las mejoras en su salud pueden variar de acuerdo al tipo de malestar, tiempo que lleve con él y el tipo de tratamiento que tome.</p>

										<li>¿Puedo continuar con mis tratamientos médicos regulares?</li>
										<p class="text-left rem--7">Siempre es recomendable que continúe con los tratamiento médicos que sigue de forma paralela a los realizados por Esfera Médica.</p>

										<li>¿Se requieren estudios clínicos?</li>
										<p class="text-left rem--7">Si es necesario, los tratamientos pueden requerir de información adicional como estudios clínicos, radiografías u otros para entender y tratar su malestar.</p>

										<li>¿Dónde debo hacerme el tratamiento?</li>
										<p class="text-left rem--7">Los tratamientos se realizan en nuestras instalaciones.</p>

										<li>¿Cuánto tiempo durará cada sesión del tratamiento?</li>
										<p class="text-left rem--7">Dependerá del tipo de malestar y el tipo de tratamiento que tome.</p>

										<li>¿Debería acompañarme un familiar a las sesiones del tratamiento?</li>
										<p class="text-left rem--7">Si lo desea puede hacerse acompañar por alguna persona de su confianza.</p>

										<li>¿Hay algún efecto secundario en particular que deba comunicarle de inmediato?</li>
										<p class="text-left rem--7">En caso de molestias derivadas del tratamiento háganoslo saber de inmediato para determinar las causas.</p>

										<li>¿Debo decirle qué medicamentos y/o suplementos estoy tomando actualmente?</li>
										<p class="text-left rem--7">Sí, cualquier información adicional a su evaluación es de gran ayuda para su tratamiento.</p>
									

									</ul>
								</div>
							</div>

						</div>
					</div>
					<div class="col-4 d-none d-lg-block"></div>
				</div>
			</div>
		</section>


		<!-- Footer -->
		<section class="position-relative" id="contact" data-parallax="scroll" data-image-src="./img/bg-12.png">
			
			<figure class="position-absolute icon-logo-em-right">
				<img class="icon-logo-about" src="./img/logo-em-2.png">
			</figure>

			<footer>
				<div class="container-fluid no-gutters section-contact" >

					<div class="row h-full">
						<div class="col-12 align-self-start mt-5">
							<div class="container">
								<h1 class="mt-5 mb-5 mark-menu-contact-1 mpro-bold"  title-mb="SUCURSALES">Sucursales</h1>
								<div class="row my-4 mpro-rg t-contact">
									
									<div class="px-4 px-lg-5 col-6 col-md-6 col-lg-3">
										<ul>
											<li class="rem-1-2 mpro-bold"><b>Esfera Médica</b></li>
											<li>World Trade Center Montecito N. 38, Piso 9, oficina 17 Col. Nápoles C. P. 03810, CDMX, Ciudad de México Tel. 8662 0732</li>
										</ul>
									</div>
									<div class="px-4 px-lg-5 col-6 col-md-6 col-lg-3">
										<ul>
											<li class="rem-1-2 mpro-bold"><b>Esfera Médica</b></li>
											<li>Del Valle Bartolache N. 1111 Col. Del Valle C. P. 03100, CDMX, Ciudad de México Tel. 5335 0030</li>
										</ul>
									</div>
									<div class="px-4 px-lg-5 col-6 col-md-6 col-lg-3">
										<ul>
											<li class="rem-1-2 mpro-bold"><b>Esfera Médica</b></li>
											<li>Ciudad Juárez 20 de noviembre N. 4957 Col. Colegio C. P. 32340, Ciudad Juárez, Chihuahua Tel. 6561 3803</li>
										</ul>
									</div>
									<div class="px-4 px-lg-5 col-6 col-md-6 col-lg-3">
										<ul>
											<li class="rem-1-2 mpro-bold"><b>Esfera Médica</b></li>
											<li>Tijuana Centro Médico Premier, Antonio Caso N. 2055, Col. Zona Urbana Río C. P. 22010, Tijuana, Baja California Tel. 664 634 1640</li>
										</ul>
									</div>

								</div>
							</div>
						</div>
					</div>
		
				</div>
			</footer>

		</section>


		
	    
		<script type="text/javascript">
			  
		    $(document).ready(function(){
				// Add smooth scrolling to all links
				$("a").on('click', function(event) {

				// Make sure this.hash has a value before overriding default behavior
				if (this.hash !== "") {

				  console.log(this.hash)

				  var itemsMenu = $('.dropdown-item');
				  itemsMenu.each(function(index, el) {
					$(el).removeClass('active')
				  });

				  var itemsMenuAlone = $('.menu-alone');
				  itemsMenuAlone.each(function(index, el) {
					$(el).removeClass('active')
				  });

				  $('[href="'+this.hash+'"]').addClass('active');

				  // Prevent default anchor click behavior
				  event.preventDefault();

				  // Store hash
				  var hash = this.hash;


				  // Using jQuery's animate() method to add smooth page scroll
				  // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
				  $('html, body').animate({
				    scrollTop: $(hash).offset().top
				  }, 600, function(){

				    // Add hash (#) to URL when done scrolling (default click behavior)
				    window.location.hash = hash;
				  });
				} // End if
				});

				(function($) {
				    $.fn.hasScrollBar = function() {
				        return this.get(0).scrollHeight > this.height();
				    }
				})(jQuery);

				function showSecction(elem){
					var element = document.querySelector(elem);
					var position = element.getBoundingClientRect();					
					if(position.top >= 0 && position.bottom <= window.innerHeight) {						
						$('#title-menu').text($(elem).attr('title-mb'));
					}
					if(position.top < window.innerHeight && position.bottom >= 0) {						
						$('#title-menu').text($(elem).attr('title-mb'));
					}
				}

				window.addEventListener('scroll', function() {
					
					showSecction('.mark-menu');
					showSecction('.mark-menu-about');
					showSecction('.mark-menu-about-1');
					showSecction('.mark-menu-tecno-1');
					showSecction('.mark-menu-tecno-2');
					showSecction('.mark-menu-tecno-3');
					showSecction('.mark-menu-tecno-4');
					showSecction('.mark-menu-tecno-5');
					showSecction('.mark-menu-especial-1');
					showSecction('.mark-menu-especial-2');
					showSecction('.mark-menu-prensa-1');
					showSecction('.mark-menu-blog-1');
					showSecction('.mark-menu-faqs-1');
					showSecction('.mark-menu-contact-1');
					showSecction('.mark-menu-videoblog-1');	
					

				});


				function checkScreenSize(size, minBG, maxBG) {
				  var x = window.matchMedia(size)
				  if (x.matches) { // If media query matches		
				    minBG();
				  } else {				
				  	maxBG();
				  }
				  x.addListener(checkScreenSize)
				}
				

				checkScreenSize('(max-width: 700px)',function(){
						
					$('#dr-Agustin-gomez').parallax({
						position:'650px 0px',
						imageSrc:'./img/9mobile.jpg'
					});
					$('#dr-ignacio-martinez').parallax({
						position:'650px 0px',
						imageSrc:'./img/10mobile.jpg'
					});
					$('#monalisa-touch').parallax({
						position:'660px 0px',
						imageSrc:'./img/7mobile.jpg'
					});

				},function(){
					
					$('#dr-Agustin-gomez').parallax({
						imageSrc:'./img/9.png'
					});
					$('#dr-ignacio-martinez').parallax({
						imageSrc:'./img/10.png'
					});
					$('#monalisa-touch').parallax({
						imageSrc:'./img/7.png'
					});

				}); 

				
				$('.dropdown-item').on('click', function(){
				    $('.navbar-collapse').collapse('hide');
				});

				$('.menu-alone').on('click', function(){
				    $('.navbar-collapse').collapse('hide');
				});


				var itemsVideoBlog = $('.item-video-blog-em');
				var itemsVideoBlogFirst = $(itemsVideoBlog[0]);
				$('#box-video').attr('src',itemsVideoBlogFirst.attr('youtube-url') );

				itemsVideoBlog.click( function(event) {					
					
					var currentElement = $(this);

					itemsVideoBlog.each(function(index, el) {
						$(el).removeClass('video-active')
					});

					currentElement.addClass('video-active');
					var url = currentElement.attr('youtube-url');

					$('#box-video').attr('src',url);
				});

				var containerInfo = $('.container-info');
				containerInfo.toggle('fast');
				$('.icon-add-circle').on('click', function(event) {			
					containerInfo.toggle('slow');
				});


			});


 			function loadScreen(){
 				$('.init-load').fadeOut('400', function() {
 					console.log('Load Page!');
 				});
 			}

		</script>


</body>
</html>